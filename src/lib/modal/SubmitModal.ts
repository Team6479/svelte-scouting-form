export interface ModalBody {
  icon?: string;
  content: string;
  html?: boolean;
}

export interface ModalData {
  body?: ModalBody;
  visible: boolean;
  resetCallback?: Function
}