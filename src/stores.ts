import { writable } from "svelte/store";
import { browser } from "$app/environment";
import type { ModalBody, ModalData } from "$lib/modal/SubmitModal";

export const layout = writable(browser && localStorage.getItem('layout') || 'column')
layout.subscribe(value => {
  if(browser) return localStorage.setItem('layout', value === 'row' ? 'row' : 'column');
});

export const theme = writable(browser && localStorage.getItem('theme') || 'dark')
theme.subscribe(value => {
  if(browser) return localStorage.setItem('theme', value === 'dark' ? 'dark' : 'light');
});

function modalStore() {
  const { subscribe, set, update } = writable<ModalData>({ visible: false })

  return {
    subscribe,
    set,
    update,
    setVisible: (visible: boolean) => update(data => {
      data.visible = visible
      return data
    }),
    setBody: (body: ModalBody) => update(data => {
      data.body = body
      return data
    }),
  }
}
export const modal = modalStore()
