import type { RequestHandler } from "@sveltejs/kit";
import type { SubmitData } from "$lib/Api";
import { Spreadsheet } from "$lib/server/Sheets";

export const POST: RequestHandler = async ({ request }) => {
	const data: SubmitData = await request.json()

	const headerRow = []
	const newRow = []
	headerRow[0] = "Timestamp"
	newRow[0] = new Date(Date.now())
	for(const period of Object.values(data.periods)) {
		for(const section of Object.values(period.sections)) {
			if(section.column !== undefined && section.column >= 0) {
				headerRow[section.column + 1] = section.name
				const rawValue = section.value != null ? section.value : section.default_value
				let value
				if(section.type === "option") {
					if(rawValue instanceof Array<number>) {
						value = []
						for(const index of rawValue) {
							value.push(section.options[index])
						}
						value = value.join(', ')
					}
					else if(typeof rawValue === 'number') {
						value = section.options[rawValue]
					}
				}
				else {
					value = rawValue
				}
				newRow[section.column + 1] = value
			}
		}
	}

	if(newRow.length > 0) {
		if(data.sheet.spreadsheetId == null) return new Response(JSON.stringify({ success: true }))
		const spreadsheet = new Spreadsheet(data.sheet.spreadsheetId)
		const rows = []
		if(data.sheet.title && !(await spreadsheet.sheetExists(data.sheet.title))) {
			await spreadsheet.create(data.sheet.title)
			rows.push(headerRow)
		}
		rows.push(newRow)
		const res = await spreadsheet.appendRows(data.sheet.title, rows)
		if(!res) {
			return new Response(JSON.stringify({
				success: false,
				error: "Unable to send data to spreadsheet"
			}))
		}
	}

	return new Response(JSON.stringify({
		success: true
	}))
}
  